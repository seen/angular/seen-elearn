/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('seen-elearn' , [ 'seen-core' ])

/**
 * @ngdoc Factories
 * @name ElCourse
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElCourse', seen.factory({
	url : '/api/v2/elearn/courses',
	// Lesson
}))

/**
 * @ngdoc Factories
 * @name ElDomain
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElDomain',seen.factory({
	url : '/api/v2/elearn/domains',
	// topics
}))

/**
 * @ngdoc Factories
 * @name ElLesson
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElLesson', seen.factory({
	url : '/api/v2/elearn/lessons',
	// topics
}))
/**
 * @ngdoc Factories
 * @name ElPart
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElPart', seen.factory({
	url : '/api/v2/elearn/parts',
	// topics
}))

/**
 * @ngdoc Factories
 * @name ElTopic
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElTopic', seen.factory({
	url : '/api/v2/elearn/topics',
}))

/**
 * @ngdoc Services
 * @name $$elearn
 * @description Manages items in $elearn system
 * 
 */
.service('$elearn', seen.service({
	resources : [ {
		name : 'Topic',
		factory : 'ElTopic',
		url : '/api/v2/elearn/topics',
		type : 'collection'
	}, {
		name : 'Lesson',
		factory : 'ElLesson',
		url : '/api/v2/elearn/lessons',
		type : 'collection'
	}, {
		name : 'Domain',
		factory : 'ElDomain',
		url : '/api/v2/elearn/domains',
		type : 'collection'
	}, {
		name : 'Part',
		factory : 'ElPart',
		url : '/api/v2/elearn/parts',
		type : 'collection'
	} ]
}));

