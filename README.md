# Seen ELearn

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-elearn/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-elearn/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/2df51f73cc44453facf644ab8312baaa)](https://www.codacy.com/app/seen/seen-elearn?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-elearn&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/2df51f73cc44453facf644ab8312baaa)](https://www.codacy.com/app/seen/seen-elearn?utm_source=gitlab.com&utm_medium=referral&utm_content=seen/angular/seen-elearn&utm_campaign=Badge_Coverage)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-elearn/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-elearn/commits/develop)